<?php

namespace App\Services;

use App\Models\Account;
use App\Models\AccountSubAccount;
use App\Exports\ValueExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Arr;

class AccountService
{
    private $account = [];

    public function data_sub_account($request)
    {
        $accounts = $this->getAccount();

        foreach ($accounts as $key => $value) {
            $this->account[$value->id] = new \stdClass();
            $this->account[$value->id]->id = $value->id;
            $this->account[$value->id]->value = ($request->value[$key] ?? 0);
        }
        $data = $this->sub_account();

        return $this->sub_account();
    }

    public function sub_account()
    {
        $account = array_map(function($item){
            $row = new \stdClass;
            $row->id = $item['id'];
            $row->name = "[ ".$item['category']['name']." ] ".$item['name'];
            $row->category = $item['category']['name'];
            $row->value = $this->account[$item['id']]->value ?? 0;

            $sub = $this->recursive_sub($item['sub_account']);
            $sub_name = $this->recursive_sub_name($sub);
            $sub_value = $this->recursive_sub_value($sub);
            $sub_operator = $this->recursive_sub_operator($sub);

            $row->sub = $sub;
            $row->sub_name = $sub_name;
            $row->sub_value = $sub_value;
            $row->text_sub_name = $this->text_formula($sub_name, $sub_operator);
            $row->text_sub_value = $this->text_formula($sub_value, $sub_operator);
            return $row;

            //return $sub;
        }, Account::where('level', 1)->with('subAccount')->get()->toArray());

        return $account;
        //return $this->recursive_sub_name($account);
    }

    public function recursive_sub($data)
    {
        $account = array_map(function($item){
            $subAccount = Account::with('subAccount')->find($item['sub_account_id'])->toArray();
            $row = new \stdClass;
            $row->id = $subAccount['id'];
            $row->name = "[ ".$subAccount['category']['name']." ] ".$subAccount['name'];
            $row->category = $subAccount['category']['name'];
            $row->value = $this->account[$subAccount['id']]->value ?? 0;
            $row->right = $item['right_operator'];
            $row->sub = $this->recursive_sub($subAccount['sub_account']);

            return $row;
        }, $data);

        return $account;
    }

    public function recursive_sub_name($sub) {
        $account = array_map(function($item) {
            if (sizeof($item->sub) > 0) {
                return $this->recursive_sub_name($item->sub);
            } else {
                return $item->name;
            }
        }, $sub);
    
        return Arr::flatten($account);
    }

    public function recursive_sub_value($sub) {
        $account = array_map(function($item) {
            if (sizeof($item->sub) > 0) {
                return $this->recursive_sub_value($item->sub);
            } else {
                return $item->value;
            }
        }, $sub);
    
        return Arr::flatten($account);
    }

    public function recursive_sub_operator($sub, $before = null) {
        $account = array_map(function($item) use($before) {
            if (sizeof($item->sub) > 0) {
                return $this->recursive_sub_operator($item->sub, ($item->right ?? $before));
            } else {
                if ($item->right != null) {
                    return $item->right;
                } else {
                    return $before;
                }
            }
        }, $sub);
    
        return Arr::flatten($account);
    }

    public function text_formula($account, $operator) {
        $text = [];
        foreach ($account as $key => $value) {
            array_push($text, $value, $operator[$key]);
        }

        $result = join(" ", $text);

        return $result;
    }

    public function dataTable($request)
    {
        request()->validate([
            'direction' => ['in:asc,desc'],
            'field' => ['in:name,account_category_id,type']
        ]);

        $query = Account::query();

        if (request('search')) {
            $query->where('name', 'LIKE', '%'.request('search').'%');
        }

        if (request()->has(['field', 'direction'])) {
            $query->orderBy(request('field'), request('direction'));
        }

        return $query->paginate(5)->withQueryString();
    }

    public function storeData($request)
    {
        $isExists = Account::where('name', $request->name)->where('type', $request->type)->where('account_category_id', $request->account_category_id)->count();

        if ($isExists > 0) {
            return redirect()->route('account.create')->withErrors(['error' => 'Data has already been taken.'], 'createAccount');
        }

        $validator = Validator::make($request->all(), [
            "name" => 'required',
            "account_category_id" => 'required',
            "type" => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->route('account.create')->withErrors($validator, 'createAccount');
        }

        $account = Account::create([
            'name' => $request->name,
            'account_category_id' => $request->account_category_id,
            'type' => $request->type,
        ]);

        foreach ($request->subaccount as $key => $item) {
            AccountSubAccount::create([
                'account_id' => $account->id,
                'sub_account_id' => $item['sub_account_id'],
                'right_operator' => $item['right_operator'],
            ]);
        }

        return redirect()->route('account.create');
    }

    public function find($id)
    {
        return Account::where('id', $id)->first();
    }

    public function findSubAccount($account_id)
    {
        return AccountSubAccount::where('account_id', $account_id)->get();
    }

    public function updateData($request, $id)
    {
        $isExists = Account::where('name', $request->name)->where('account_category_id', $request->account_category_id)->count();

        if ($isExists > 1) {
            return redirect()->route('account.edit', $id)->withErrors(['error' => 'Data has already been taken.'], 'updateAccount');
        }

        $validator = Validator::make($request->all(), [
            "name" => 'required',
            "account_category_id" => 'required',
            "type" => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->route('account.edit', $id)->withErrors($validator, 'updateAccount');
        }

        $update = Account::find($id);
        $update->name = $request->name;
        $update->account_category_id = $request->account_category_id;
        $update->type = $request->type;
        $update->save();

        AccountSubAccount::where('account_id', $id)->delete();
        foreach ($request->subaccount as $key => $item) {
            AccountSubAccount::create([
                'account_id' => $id,
                'sub_account_id' => $item['sub_account_id'],
                'right_operator' => $item['right_operator'],
            ]);
        }

        return redirect()->route('account.edit', [$id]);
    }

    public function deleteData($id)
    {
        try {
            $delete = Account::find($id);
            $deleted = $delete;
            $delete->delete();

            return redirect()->route('account.index');
        } catch (\Throwable $th) {
            return redirect()->back()->withErrors(['error' => 'Gagal menghapus data'], 'deleteAccount');
        }
    }

    public function getSubAccount()
    {
        return Account::where('type', 'sub')->get();
    }

    public function getAccount()
    {
        return Account::get();
    }

    public function exportExcelAccountValue($request)
    {
        $accounts = $this->getAccount();

        foreach ($accounts as $key => $value) {
            $this->account[$value->id] = new \stdClass();
            $this->account[$value->id]->id = $value->id;
            $this->account[$value->id]->name = " [ ".$value->category->name." ] ".$value->name;
            $this->account[$value->id]->value = ($request->value[$key] ?? 0);
        }

        $data = $this->sub_account();

        return (new ValueExport($data))->download('account-values.xlsx');
    }

    public function readExcelAccountValue($request)
    {
        $array = Excel::toArray(new \stdClass(), $request->file('file'));

        $data = [];

        foreach ($array[0] as $key => $value) {
            if ($value[0] !== null) {
                $data[$value[0]] = array('id' => $value[0], 'value' => $value[7]);
            }
        }

        $account = Account::get();
        $account_value = [];
        foreach ($account as $key => $value) {
            if (array_key_exists($value->id, $data)) {
                $account_value[] = ['id' => $value->id, 'value' => $data[$value->id]['value']];
            } else {
                $account_value[] = ['id' => $value->id, 'value' => 0];
            }
        }

        return $account_value;
    }

    public function compareAccountMainSub($request)
    {
        $account = $this->getAccount();
        
        foreach ($account as $key => $value) {
            $this->account[$value->id] = $value;
            $this->account[$value->id]->val = $request->value[$key];
        }

        $mainAccount = Account::with('subAccount')->where('type', 'main')->get()->toArray();
        $data = array_map(function($item) use($account){
            $row = new \stdClass;
            $row->id = $item["id"];
            $row->name = $item["name"];
            $row->category = $item["category"]["name"];
            $row->value = $this->account[$item["id"]]->val;

            $textSubAccount = "";
            $valueSubAccount = "";
            $accountSubAccount = [];
            $accountSubAccount= [];
            $subAccountName = [];
            $subAccountValue= [];
            foreach ($item['sub_account'] as $key => $value) {
                if ($value !== null) {
                    $textSubAccount .= $value['sub_account']['name']." ".$value['right_operator']." ";
                    $valueSubAccount .= $this->account[$value['sub_account_id']]->val." ".$value['right_operator']." "; 
                    $accountSubAccount[] = $value['sub_account']['name'];
                    $accountSubAccount[] = $this->account[$value['sub_account_id']]->val;
                    $subAccountName[] = $value['sub_account']['name']." ( ".$value['sub_account']['category']['name']." ) ";
                    $subAccountValue[] = $this->account[$value['sub_account_id']]->val;
                }
            }

            $row->textSubAccount = $textSubAccount;
            $row->valueSubAccount = $valueSubAccount; 
            $row->accountSubAccount = $accountSubAccount;
            $row->accountSubAccount = $accountSubAccount;
            $row->subAccountName = $subAccountName;
            $row->subAccountValue = $subAccountValue;
            return $row;
        }, $mainAccount);

        return $data;
    }
}