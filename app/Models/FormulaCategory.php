<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FormulaCategory extends Model
{
    use HasFactory;

    protected $table = 'formula_categories';

    protected $fillable = [
        'name',
    ];
}
