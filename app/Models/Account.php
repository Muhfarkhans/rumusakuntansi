<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    use HasFactory;

    protected $table = 'accounts';
    
    protected $fillable = [
        'account_category_id',
        'name',
        'type',
        'level',
        'code',
    ];

    protected $appends = ['value'];

    public function getValueAttribute()
    {
        return 0;
    }

    protected $with = ['category:id,name'];

    public function category()
    {
        return $this->hasOne(AccountCategory::class, 'id', 'account_category_id');
    }

    public function subAccount()
    {
        return $this->hasMany(AccountSubAccount::class);
    }

}
