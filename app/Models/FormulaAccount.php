<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FormulaAccount extends Model
{
    use HasFactory;

    protected $table = 'formula_accounts';

    protected $fillable = [
        'formula_id',
        'account_id',
        'right_operator',
    ];

    protected $with = ['account'];

    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    public function formula()
    {
        return $this->belongsTo(Formula::class);
    }
}
