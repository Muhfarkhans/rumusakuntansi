<?php

namespace App\Http\Controllers;

use App\Models\AccountCategory;
use App\Services\AccountService;
use Illuminate\Http\Request;
use Inertia\Inertia;

class AccountController extends Controller
{
    public function index(Request $request, AccountService $accountService)
    {
        $datatable = $accountService->datatable($request);

        return Inertia::render('Account/ListAccount', [
            'accounts' => $datatable,
            'filters' => request()->all(['search', 'field', 'direction']),
            'base_url' => url(''),
        ]);
    }

    public function create(AccountService $accountService)
    {
        $category = AccountCategory::all();

        // get account bertipe sub
        $subaccounts = $accountService->getSubAccount();;

        return Inertia::render('Account/CreateAccountFormTest', [
            'category' => $category,
            'subaccounts' => $subaccounts,
        ]);
    }

    public function store(Request $request, AccountService $accountService)
    {
        return $accountService->storeData($request);
    }

    public function edit(AccountService $accountService, $id)
    {
        $account = $accountService->find($id);
        $subaccount = $accountService->findSubAccount($id);

        $subaccounts = $accountService->getSubAccount();
        $categoryAccount = AccountCategory::all();

        return Inertia::render('Account/EditAccountForm', [
            'account' => $account,
            'subaccount' => $subaccount,
            'subaccounts' => $subaccounts,
            'category' => $categoryAccount,
        ]);
    }

    public function update(Request $request, AccountService $accountService, $id)
    {
        return $accountService->updateData($request, $id); 
    }

    public function detail(AccountService $accountService, $id)
    {
        $account = $accountService->find($id);

        return $account;

        return Inertia::render('Account/DetailAccount', [
            'account' => $account,
        ]);
    }

    public function delete(AccountService $accountService, $id)
    {
        return $accountService->deleteData($id);
    }
}
