<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccountSubAccount extends Model
{
    use HasFactory;

    protected $table = 'account_sub_accounts';
    
    protected $fillable = [
        'account_id',
        'sub_account_id',
        'right_operator',
    ];

    protected $with = ['sub_account'];

    public function account()
    {
        return $this->belongsTo(Account::class);
    }
    
    public function sub_account()
    {
        return $this->belongsTo(Account::class);
    }
}
