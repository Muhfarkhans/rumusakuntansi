<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Formula extends Model
{
    use HasFactory;

    protected $table = 'formulas';

    protected $fillable = [
        'account_id',
        'formula_category_id',
    ];

    protected $with = ['category', 'account'];

    public function category()
    {
        return $this->hasOne(FormulaCategory::class, 'id', 'formula_category_id');
    }

    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    public function formulaAccount()
    {
        return $this->hasMany(FormulaAccount::class);
    }
}
