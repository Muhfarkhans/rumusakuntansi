<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ValueExport implements FromView, WithColumnWidths
{
    use Exportable;

    public function __construct(Array $account) {
        $this->account = $account;
    }

    public function columnWidths(): array
    {
        return [
            'A' => 5,
            'B' => 5,
            'C' => 5,
            'D' => 5,
            'E' => 5,
            'F' => 5,
            'G' => 70,   
            'H' => 50   
        ];
    }

    public function view() : View
    {
        return view('exports.excel-value', [
            'account' => $this->account
        ]);
    }
}
