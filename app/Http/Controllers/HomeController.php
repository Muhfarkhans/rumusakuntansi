<?php

namespace App\Http\Controllers;

use App\Services\AccountService;
use App\Services\FormulaService;
use App\Imports\ValueImport;
use Illuminate\Http\Request;
use Inertia\Inertia;

class HomeController extends Controller
{
    public function index(AccountService $accountService)
    {
        $accounts = $accountService->getAccount();

        return Inertia::render('Home/InputAccount', [
            'accounts' => $accounts
        ]);
    }

    public function report(Request $request, FormulaService $formulaService, AccountService $accountService)
    {
        $formula = $formulaService->generateResult($request);
        $category = $formulaService->getCategory();
        $account = $accountService->data_sub_account($request);

        return Inertia::render('Home/ReportFormula', [
            'formula' => $formula,
            'category' => $category,
            'account' => $account,
        ]);
    }

    public function exportValue(Request $request, AccountService $accountService) {
        return $accountService->exportExcelAccountValue($request);
    }

    public function uploadValue(Request $request, AccountService $accountService) {
        return $accountService->readExcelAccountValue($request);
    }

    public function exportReportExcel(Request $request, FormulaService $formulaService) {
        return $formulaService->exportExcelReport($request);
    }
}
