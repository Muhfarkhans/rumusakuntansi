<?php

use App\Http\Controllers\AccountController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\SubAccountController;
use App\Http\Controllers\FormulaController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('home');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->name('dashboard');

/* Route::middleware(['auth:sanctum', 'verified'])->name('account.')->prefix('account/')->group(function() {
    Route::get('/', [AccountController::class, 'index'])->name('index');
    Route::get('/detail/{id}', [AccountController::class, 'detail'])->name('detail');
    Route::get('/create', [AccountController::class, 'create'])->name('create');
    Route::post('/store', [AccountController::class, 'store'])->name('store');
    Route::get('/edit/{id}', [AccountController::class, 'edit'])->name('edit');
    Route::patch('/update/{id}', [AccountController::class, 'update'])->name('update');
    Route::delete('/delete/{id}', [AccountController::class, 'delete'])->name('delete');
});

Route::middleware(['auth:sanctum', 'verified'])->name('formula.')->prefix('formula/')->group(function() {
    Route::get('/', [FormulaController::class, 'index'])->name('index');
    Route::get('/detail/{id}', [FormulaController::class, 'detail'])->name('detail');
    Route::get('/create', [FormulaController::class, 'create'])->name('create');
    Route::post('/store', [FormulaController::class, 'store'])->name('store');
    Route::get('/edit/{id}', [FormulaController::class, 'edit'])->name('edit');
    Route::patch('/update/{id}', [FormulaController::class, 'update'])->name('update');
    Route::delete('/delete/{id}', [FormulaController::class, 'delete'])->name('delete');
}); */

Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/report', function() {return redirect()->route('home');});
Route::post('/report', [HomeController::class, 'report'])->name('report');
Route::post('/export-value', [HomeController::class, 'exportValue'])->name('export-value');
Route::post('/read-value-excel', [HomeController::class, 'uploadValue'])->name('read-value-excel');
Route::post('/export-report-excel', [HomeController::class, 'exportReportExcel'])->name('export-report-excel');
