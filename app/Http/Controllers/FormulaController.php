<?php

namespace App\Http\Controllers;

use App\Services\FormulaService;
use Illuminate\Http\Request;
use Inertia\Inertia;

class FormulaController extends Controller
{
    public function index(Request $request, FormulaService $formulaService)
    {   
        $datatable = $formulaService->dataTable($request);

        return Inertia::render('Formula/ListFormula', [
            'formula' => $datatable,
            'filters' => request()->all(['search', 'field', 'direction']),
            'base_url' => url(''),
        ]);
    }

    public function create(FormulaService $formulaService)
    {
        $category = $formulaService->getCategory();
        $accounts = $formulaService->getAccount();

        return Inertia::render('Formula/CreateFormulaForm', [
            'category' => $category,
            'accounts' => $accounts,
        ]);
    }

    public function store(Request $request, FormulaService $formulaService)
    {
        return $formulaService->storeData($request);
    }

    public function detail(FormulaService $formulaService, $id)
    {
        $formula = $formulaService->findFormula($id);

        //return response()->json($formula);

        return Inertia::render('Formula/DetailFormula', [
            'formula' => $formula
        ]);
    }

    public function edit(FormulaService $formulaService, $id)
    {
        $category = $formulaService->getCategory();
        $accounts = $formulaService->getAccount();
        $formula = $formulaService->findformula($id);

        return Inertia::render('Formula/EditFormulaForm', [
            'category' => $category,
            'accounts' => $accounts,
            'formula' => $formula,
        ]);
    }

    public function update(Request $request, FormulaService $formulaService, $id)
    {
        return $formulaService->updateData($request, $id); 
    }

    public function delete(FormulaService $formulaService, $id)
    {
        return $formulaService->deleteData($id);
    }
}
