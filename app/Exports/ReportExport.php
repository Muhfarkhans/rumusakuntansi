<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ReportExport implements FromView, WithColumnWidths, WithStyles
{
    use Exportable;

    public function __construct(Array $formula) {
        $this->formula = $formula;
    }

    public function columnWidths(): array
    {
        return [
            'B' => 100,
            'C' => 50,
        ];
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('A')->getAlignment()->setVertical('center');
        $sheet->getStyle('A')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('B')->getAlignment()->setWrapText(true);
    }

    public function view() : View
    {
        return view('exports.excel-report', [
            'formula' => $this->formula
        ]);
    }
}