<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccountCategory extends Model
{
    use HasFactory;

    protected $table = 'account_categories';

    protected $fillable = [
        'name',
    ];

    public function account()
    {
        return $this->hasMany(Account::class);
    }
}
