<?php

namespace App\Services;

use App\Models\Account;
use App\Models\Formula;
use App\Models\FormulaAccount;
use App\Models\FormulaCategory;
use App\Exports\ReportExport;
use Illuminate\Support\Facades\Validator;

class FormulaService
{

    private $account = [];

    public function getAccount()
    {
        return Account::all(); 
    }

    public function dataTable($request)
    {
        request()->validate([
            'direction' => ['in:asc,desc'],
            'field' => ['in:name,formula_category_id']
        ]);

        $query = Formula::query();

        if (request('search')) {
            $query->whereHas('account', function($account) use($request) {
                $account->where('name', 'LIKE', '%'.request('search').'%');
            });
        }

        if (request()->has(['field', 'direction'])) {
            if (request('field') == 'name') {
                $query->whereHas('account', function($account) use($request) {
                    $account->orderBy(request('field'), request('direction'));
                });
            } else {
                $query->orderBy(request('field'), request('direction'));
            }
        }

        return $query->paginate(5)->withQueryString();
    }

    public function storeData($request)
    {
        $validator = Validator::make($request->all(), [
            "account_id" => 'required',
            "formula_category_id" => 'required',
            "formula_account" => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->route('formula.create')->withErrors($validator, 'createFormula');
        }

        $formula = Formula::create([
            "account_id" => $request->account_id,
            "formula_category_id" => $request->formula_category_id
        ]);

        foreach ($request->formula_account as $key => $value) {
            FormulaAccount::create([
                "formula_id" => $formula->id,
                "account_id" => $value['account_id'],
                "right_operator" => $value['right_operator'],
            ]);
        }

        return redirect()->route('formula.create');
    }

    public function findFormula($id)
    {
        return Formula::with('formulaAccount')->where('id', $id)->first();
    }

    public function updateData($request, $id)
    {
        $validator = Validator::make($request->all(), [
            "account_id" => 'required',
            "formula_category_id" => 'required',
            "formula_account" => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->route('formula.edit', $id)->withErrors($validator, 'updateFormula');
        }

        $update = Formula::find($id);
        $update->account_id = $request->account_id;
        $update->formula_category_id = $request->formula_category_id;
        $update->save();

        FormulaAccount::where('formula_id', $id)->delete();
        foreach ($request->formula_account as $key => $item) {
            FormulaAccount::create([
                'formula_id' => $id,
                'account_id' => $item['account_id'],
                'right_operator' => $item['right_operator'],
            ]);
        }

        return redirect()->route('formula.edit', [$id]);
    }

    public function deleteData($id)
    {
        try {
            $delete = Formula::find($id);
            $deleted = $delete;
            $delete->delete();

            return redirect()->route('formula.index');
        } catch (\Throwable $th) {
            return redirect()->back()->withErrors(['error' => 'Gagal menghapus data'], 'deleteFormula');
        }
    }

    public function getFormula()
    {
        return Formula::with('formulaAccount')->get();
    }

    public function getCategory()
    {
        return FormulaCategory::get();
    }

    public function generateResult($request)
    {
        $account = $this->getAccount();
        
        foreach ($account as $key => $value) {
            $this->account[$value->id] = $value;
            $this->account[$value->id]->val = $request->value[$key];
        }

        $formula = $this->getFormula()->toArray();
        $data = array_map(function($item) use($account){
            $row = new \stdClass;
            $row->id = $item["id"];
            $row->title = $item["account"]["name"];
            $row->category = $item["category"]["name"];
            $row->value = $this->account[$item["account_id"]]->val;

            $textFormula = "";
            $valueFormula = "";
            $accountName = [];
            $accountValue = [];
            foreach ($item['formula_account'] as $key => $value) {
                $textFormula .= $value['account']['name']." ".$value['right_operator']." ";
                $valueFormula .= $this->account[$value['account_id']]->val." ".$value['right_operator']." "; 
                $accountName[] = $value['account']['name'];
                $accountValue[] = $this->account[$value['account_id']]->val;
            }

            $row->textFormula = $textFormula;
            $row->valueFormula = $valueFormula; 
            $row->accountName = $accountName;
            $row->accountValue = $accountValue;
            return $row;
        }, $formula);

        return $data;
    }

    public function exportExcelReport($request)
    {
        $data = $this->generateResult($request);

        return (new ReportExport($data))->download('report.xlsx');
    }
}