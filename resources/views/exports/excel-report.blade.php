<table style="border-collapse: collapse; ">
    @foreach ($formula as $key=>$item)
        <tr>
            <td rowspan="{{ sizeof($item->accountName)+3 }}" style="background-color: orange; text-align: left;"> {{ $key+1 }}. </td>
            <td style="background-color: orange; text-align: left; font-style: italic; font-weight: bold"> {{ $item->title." = ".$item->textFormula }} </td>
            <td style="text-align: left;"> {{ $item->value }} </td>
        </tr>
        @foreach ($item->accountName as $key=>$name)
            <tr style="border-bottom: 1px solid black">
                <td style="text-align: left; font-weight: bold; padding-left: 30px;"> {{ $name }} </td>
                <td style="text-align: left; font-weight: bold;"> {{ $item->accountValue[$key] }} </td>
            </tr>
        @endforeach
        <tr style="background-color: whitesmoke">
            <td style="text-align: left;"> Selisih </td>
            <td style="text-align: left; color: whitesmoke; background-color: {{ ( ( $item->value - abs(eval(("return ".$item->valueFormula.";"))) ) == 0 ? "green" : "red" ) }} ;"> 
                {{ ( $item->value - abs(eval(("return ".$item->valueFormula.";"))) ) }} 
            </td>
        </tr>
        <tr style="background-color: whitesmoke">
            @php
                $selisih = $item->value - abs(eval(("return ".$item->valueFormula.";")));
                $pesan = "";
                if ($selisih == 0) {
                    $pesan = "Nilai ".$item->title." sama dengan nilai dari pembandingnya";
                } else if ($selisih < 0) {
                    $pesan = "Nilai ".$item->title." kurang ".abs($selisih)." dari pembandingnya";
                } else {
                    $pesan = "Nilai ".$item->title." lebih banyak ".abs($selisih)." dari pembandingnya";
                }
                
            @endphp
            <td colspan="2" style="text-align: left;"> {{ $pesan }} </td>
        </tr>
        <tr>
            <td style="padding-bottom: 10px"></td>
        </tr>
    @endforeach
</table>