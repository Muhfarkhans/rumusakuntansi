<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToArray;

class ValueImport implements ToArray
{
    private $data;

    public function __construct()
    {
        $this->data = [];
    }
    public function array(array $rows)
    {
        foreach ($rows as $row) {
            $this->data[] = array('id' => $row[0], 'value' => $row[7]);
        }
    }

    public function getArray(): array
    {
        return $this->data;
    }
}
