<table style="border-collapse: collapse; ">
    @foreach ($account as $key=>$item)
        <tr>
            <td style="text-align: left;"> {{ $item->id }}. </td>
            <td style="text-align: left; font-style: italic; font-weight: bold"> {{ $item->name }} </td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td style="text-align: left;"> {{ $item->value }} </td>
        </tr>
        @foreach ($item->sub as $a=>$aa)
        <tr>
            <td style="text-align: left;"> {{ $aa->id }}. </td>
            <td></td>
            <td>{{ $aa->name }}</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td style="text-align: left;"> {{ $aa->value }} </td>
        </tr>
            @foreach ($aa->sub as $b=>$bb)
            <tr>
                <td style="text-align: left;"> {{ $bb->id }}. </td>
                <td></td>
                <td></td>
                <td>{{ $bb->name }}</td>
                <td></td>
                <td></td>
                <td></td>
                <td style="text-align: left;"> {{ $bb->value }} </td>
            </tr>
                @foreach ($bb->sub as $c=>$cc)
                <tr>
                    <td style="text-align: left;"> {{ $cc->id }}. </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>{{ $cc->name }}</td>
                    <td></td>
                    <td></td>
                    <td style="text-align: left;"> {{ $cc->value }} </td>
                </tr>
                    @foreach ($cc->sub as $d=>$dd)
                    <tr>
                        <td style="text-align: left;"> {{ $dd->id }}. </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>{{ $dd->name }}</td>
                        <td></td>
                        <td style="text-align: left;"> {{ $dd->value }} </td>
                    </tr>
                        @foreach ($dd->sub as $e=>$ee)
                        <tr>
                            <td style="text-align: left;"> {{ $ee->id }}. </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>{{ $ee->name }}</td>
                            <td style="text-align: right;"> {{ $ee->value }} </td>
                        </tr>
                        
                        @endforeach
                    @endforeach
                @endforeach
            @endforeach
        @endforeach
        <tr></tr>
    @endforeach
</table>